import React from 'react';
import { experiencesConfig } from '../domains/experience/repository/Experience'
import { TimelinePoint } from './components/TimelinePoint/timelinePoint';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Intergiew
        </p>
      </header>

      <main>
        <div className='timeline-container'>
          <div className='timeline-vertical-line'></div>
          <div className='timeline-wrapper'>

            {
              experiencesConfig.map(({ title, type }) => <TimelinePoint side={type === 'personal' ? 'left' : 'right'} title={title} description={title} />)
            }
          </div>
        </div>
      </main>
    </div>
  );
}

export default App;
