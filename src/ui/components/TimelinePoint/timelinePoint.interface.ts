export type TimelinePointProps = {
  side: 'left' | 'right';
  title: string;
  description: string;
  expandedContent?: JSX.Element;
}