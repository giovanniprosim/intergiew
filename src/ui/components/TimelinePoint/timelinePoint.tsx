import { TimelinePointProps } from './timelinePoint.interface'
import './timelinePoint.scss'

const TimelinePoint = ({ side, title, description, expandedContent }: TimelinePointProps) => {
  return <div className={`timeline-point-container container-${side}`}>
    <div className='title'>{title}</div>
    <div className='point'>0</div>
  </div>
}

export { TimelinePoint }