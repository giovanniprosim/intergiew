import { ExperienceConfig } from "../entities/GetExperience";

export const experiencesConfig: ExperienceConfig[] = [
  {
    type: 'personal',
    title: 'Easy Pic',
    description: 'Software gerador de relatório, utilizando imagens dentro de uma pasta e organizando elas em um relatório gerado automaticamente.',
    highlights: ["Electron.js", "Economia de tempo e facilidade para quem usa", "Versões para Windows, Mac e Linux"],
    references: {
      video: "https://www.youtube.com/watch?v=5ZCUEiyNwPA",
      product: "https://rosimcorporation.com/easypic/"
    },
    technologies: ["electron"],
  },
  {
    type: 'professional',
    title: 'Pismo',
    description: "Como engenheiro frontend, o desafio é trabalhar com <b>micro frontends</b> independente do framework, criando soluções de <b>Design System</b> para todo o ecossistema.",
    highlights: [
      "Responsável pela implementação dos testes em Cypress",
      "Utilizando e mantendo o Design System agnóstico à framework",
      "Comunicação com as áreas responsáveis pelas as etapas do projeto (negócio, UX e desenvolvimento)",
      "Uso do inglês no dia-a-dia (apresentações e reuniões com equipes estrangeiras)"
    ],
    technologies: ['react', 'vuejs', 'cypress']
  },
  {
    type: 'personal',
    title: 'Rosim Engenharia WebSite',
    description: "O desafio foi o cliente poder editar todo o conteúdo, fonts e cores do site, além de poder criar páginas dinamicamente.",
    references: {
      website: "https://www.rosimengenharia.com.br/"
    },
    highlights: ["SSR", "Contentful feito para o cliente adicionar o conteúdo sozinho"],
    technologies: ['contentful', 'nextjs', 'materialUi']
  },
  {
    type: 'professional',
    title: 'Accenture - Meetups',
    description: "A empresa dava oportunidade para nós fazermos apresentações para as pessoas, sobre temas técnicos. Para minhas apresentações, usei como base artigos do meu blog <a href='https://prog-amando.vercel.app/' target='_blank'>ProgAmando</a>.",
    highlights: ["<a href='https://prog-amando.vercel.app/blog/theme-switcher-styled-components-x-variaveis-no-css' target='_blank'>Theme Switcher</a>", "<a href='https://prog-amando.vercel.app/blog/react-hooks-evitando-renders-desnecessarios' target='_blank'>Evitando Rerenders desnecessários</a>"]
  },
  {
    type: 'professional',
    title: 'Accenture - CAR OI',
    description: "Sistema de Cobranças à Receber (CAR) da empresa OI. Onde implementei todos os <b>testes E2E</b> e me aprofundei no <b>Material UI</b>.",
    highlights: ["Typescript", "Material UI (Destaque: Data Grid e Table)", "Formik (formulário) com tratamento nos inputs para evitar lag nos inputs", "Testes com Cypress"],
    technologies: ['react', 'materialUi', 'cypress']
  },
  {
    type: 'personal',
    title: 'ProgAmando',
    description: "Blog com artigos para me ajudar a reforçar meus estudos. Como <b>desafio</b>, trabalhar com mudança de temas, evitando renderizações desnecessárias, através da tag HTML com o nome do tema e variáveis CSS.",
    references: {
      git: "https://gitlab.com/next.js-projects/prog_amando",
      website: "https://prog-amando.vercel.app"
    },
    highlights: ["Mudança de temas, evitando renderizações"],
    technologies: ['nextjs', 'contentful']
  },
  // {
  //   type: 'personal',
  //   title: 'Pandemusic',
  //   description: "Catálogo das músicas que gravei durante o período de pandemia. Foi o 1º projeto pessoal que usei <b>Typescript</b>",
  //   references: {
  //     website: "https://pandemusic.vercel.app/",
  //   },
  //   highlights: ["Typescript", "Mobile First"],
  //   technologies: ["react", "nextjs"]
  // },
  {
    type: 'personal',
    title: 'Chefe Gio',
    description: "Blog de receitas e textos próprios. Sendo uma das primeiras experiências utilizando <b>Next.js</b> com <b>Contentful</b>.",
    references: {
      git: "https://gitlab.com/next.js-projects/chefe-gio",
      website: "https://chefe-gio.vercel.app",
    },
    highlights: ["Next.js", "Contentful", "Mobile First"],
    technologies: ['nextjs', 'contentful']
  },
  {
    type: 'personal',
    title: 'Natura Chronos',
    description: "Projeto realizado junto com a empresa de criação digital <a href='https://film3.com.br/' target='_blank'>Film3</a>, desenvolvendo todo o código e interagindo diretamente com a equipe de design gráfico.",
    /* (QUIZ: Tônico Detox Hidratante) */
    highlights: ["E-learning", "SCORM", "Freelancer"],
    references: {
      website: "https://chronos-homolog.netlify.app/" // homologação
    },
    technologies: ["react"]
  },
  {
    type: 'personal',
    title: 'Portfólio GDev',
    description: "Um dos meus primeiros grandes projetos, para usar como portfólio, para expor <b>meus projetos pessoais e profissionais</b>, assim como <b>currículo</b>, <b>contato</b> e algumas <b>informações pessoais</b>.",
    highlights: ["React com classes", "Website multilinguagem"],
    references: {
      website: "https://gdev.netlify.app/",
    },
    technologies: ['react']
  },
]
