type ExperienceTechnologies = 'react' | 'nextjs' | 'vuejs' | 'cypress' | 'materialUi' | 'contentful' | 'electron'

export type ExperienceConfig = {
  type: 'personal' | 'professional';
  title: string; // inner html
  description?: string; // inner html
  highlights?: string[]; // inner html
  avatar?: string; // img url
  references?: {
    git?: string;
    website?: string;
    video?: string;
    product?: string;
  },
  technologies?: ExperienceTechnologies[];
}